<?php get_header(); ?>
<div id="stage">
	<div id="col-left">
		<?php include (TEMPLATEPATH . '/inc/loop-posts.php' ); ?>
	</div>
	<div id="col-right">
		<?php if ( !function_exists('dynamic_sidebar')
		|| !dynamic_sidebar('posts-widgets') ) : ?>
		<?php endif; ?>
	</div>
	<div class="clear"></div>
</div>
<?php get_footer(); ?>