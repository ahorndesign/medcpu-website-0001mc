<?php
add_filter('widget_text', 'do_shortcode');
	
	// Add RSS links to <head> section
	automatic_feed_links();
	
	// Load jQuery
	if ( !is_admin() ) {
	   wp_deregister_script('jquery');
	   wp_register_script('jquery', ("http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"), false);
	   wp_enqueue_script('jquery');
	}

	// Dropdown menu scripts
	function add_our_scripts() {
	    if (!is_admin()) { // Add the scripts, but not to the wp-admin section.
	    // Adjust the below path to where scripts dir is, if you must.
	    $scriptdir = get_bloginfo('template_url')."/js/";

	    // Remove the wordpresss inbuilt jQuery.
	    wp_deregister_script('jquery');

	    // Lets use the one from Google AJAX API instead.
	    wp_register_script( 'jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js', false, '1.4.2');
	    // Register the Superfish javascript file
	    wp_register_script( 'superfish', $scriptdir.'sf/sf.js', false, '1.4.8');
	    // Now the superfish CSS
	    wp_register_style( 'superfish-css', $scriptdir.'sf/superfish.css', false, '1.4.8');

	    //load the scripts and style.
	    wp_enqueue_script('jquery');
	    wp_enqueue_script('superfish');
	    wp_enqueue_style('superfish-css');
	    } // end the !is_admin function
	} //end add_our_scripts function

	//Add our function to the wp_head. You can also use wp_print_scripts.
	add_action( 'wp_head', 'add_our_scripts',0);
	
	// Clean up the <head>
	function removeHeadLinks() {
    	remove_action('wp_head', 'rsd_link');
    	remove_action('wp_head', 'wlwmanifest_link');
    }
    add_action('init', 'removeHeadLinks');
    remove_action('wp_head', 'wp_generator');
    
    if (function_exists('register_sidebar')) {
    	register_sidebar(array(
    		'name' => 'Sidebar Widgets',
    		'id'   => 'sidebar-widgets',
    		'description'   => 'These are widgets for the sidebar.',
    		'before_widget' => '<div id="%1$s" class="widget %2$s">',
    		'after_widget'  => '</div>',
    		'before_title'  => '<h3>',
    		'after_title'   => '</h3>'
    	));

		register_sidebar(array(
    		'name' => 'Footer Area', 'footer-widgets',
    		'id'   => 'footer-widgets',
    		'description'   => 'These are widgets for the footer.',
    		'before_widget' => '<div id="%1$s" class="widget %2$s">',
    		'after_widget'  => '</div>',
    		'before_title'  => '<h2>',
    		'after_title'   => '</h2>'
    	));

		register_sidebar(array(
			'name' => 'Homepage Area', 'homepage-widgets',
			'id'   => 'homepage-widgets',
			'description'   => 'These are widgets at the homepage right.',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h3>',
			'after_title'   => '</h3>'
		));

		register_sidebar(array(
			'name' => 'Posts Widget Area', 'posts-widgets',
			'id'   => 'posts-widgets',
			'description'   => 'These are widgets for the side of the posts.',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h2>',
			'after_title'   => '</h2>'
		));

    }

	// thumbs

		if ( function_exists( 'add_theme_support' ) ) { // Added in 2.9
		    add_theme_support( 'post-thumbnails' );
		    set_post_thumbnail_size( 150, 150, true ); // Normal post thumbnails
		    add_image_size( 'single-post-thumbnail', 400, 9999 ); // Permalink thumbnail size
		}

	// Menus
	add_action( 'init', 'register_my_menus' );

	function register_my_menus() {
	register_nav_menus(
	array(
	'menu-main' => __( 'Main Menu' )
	)
	);
	register_nav_menus(
	array(
	'menu-footer' => __( 'Footer Menu' )
	)
	);
	}

// shortcodes


function divider_func($atts, $content = null) {
    extract(shortcode_atts(array(
        "type" => 'bottom'
    ), $atts));
    return '<div class="divider-'.$type.'"></div>'.$content.'';
}
add_shortcode( 'divider', 'divider_func' );

function butsmall_func($atts, $content = null) {
    extract(shortcode_atts(array(
        "width" => '120',
		"href" => ''
    ), $atts));
    return '<a href="'.$href.'" class="button-small" style="display: block; width: '.$width.'px;">'.$content.'</a>';
}
add_shortcode( 'button-small', 'butsmall_func' );

function butlarge_func($atts, $content = null) {
    extract(shortcode_atts(array(
        "width" => '180',
		"href" => ''
    ), $atts));
    return '<a href="'.$href.'" class="button-large" style="display: block; width: '.$width.'px;">'.$content.'</a>';
}
add_shortcode( 'button-large', 'butlarge_func' );

// limit characters
function limit_words($string, $word_limit) {
 
	// creates an array of words from $string (this will be our excerpt)
	// explode divides the excerpt up by using a space character
 
	$words = explode(' ', $string);
 
	// this next bit chops the $words array and sticks it back together
	// starting at the first word '0' and ending at the $word_limit
	// the $word_limit which is passed in the function will be the number
	// of words we want to use
	// implode glues the chopped up array back together using a space character
 
	return implode(' ', array_slice($words, 0, $word_limit));
 
}

// Add custom CTA styles to TinyMCE editor
if ( ! function_exists('tdav_css') ) {
	function tdav_css($wp) {
		$wp .= ',' . get_bloginfo('stylesheet_directory') . '/css/tinymce.css';
		return $wp;
	}
}
add_filter( 'mce_css', 'tdav_css' );


?>