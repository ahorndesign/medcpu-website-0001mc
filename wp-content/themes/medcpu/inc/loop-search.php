<?php if (have_posts()) : ?>

	<h2>Search Results</h2>

	
	<div class="category-item" id="post-<?php the_ID(); ?>">
		
		<div class="category-title">
			<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
		</div>
		<div class="category-summary"><?php the_excerpt(); ?></div>
		<div class="category-meta">
			<div class="category-button"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">Read more</a></div>
				<span><?php the_time('F jS, Y') ?>
				in <?php the_category(', '); ?> by <?php the_author(); ?></span>
		</div>
		

	</div>

	<?php include (TEMPLATEPATH . '/inc/nav.php' ); ?>
	

<?php else : ?>

	<h2>No posts found.</h2>

<?php endif; ?>