<?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>

<?php /* If this is a category archive */ if (is_category()) { ?>
	<h1><?php single_cat_title(); ?></h1>

<?php /* If this is a tag archive */ } elseif( is_tag() ) { ?>
	<h2>Posts Tagged &#8216;<?php single_tag_title(); ?>&#8217;</h2>

<?php /* If this is a daily archive */ } elseif (is_day()) { ?>
	<h2>Archive for <?php the_time('F jS, Y'); ?></h2>

<?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
	<h2>Archive for <?php the_time('F, Y'); ?></h2>

<?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
	<h2 class="pagetitle">Archive for <?php the_time('Y'); ?></h2>

<?php /* If this is an author archive */ } elseif (is_author()) { ?>
	<h2 class="pagetitle">Author Archive</h2>

<?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
	<h2 class="pagetitle">Blog Archives</h2>

<?php } ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	<div class="category-item" id="post-<?php the_ID(); ?>">
		
		<div class="category-title">
			<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
		</div>
		<div class="category-summary"><?php the_excerpt(); ?></div>
		<div class="category-meta">
			<div class="category-button"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">Read more</a></div>
				<span><?php the_time('F jS, Y') ?>
				in <?php the_category(', '); ?> <!-- Autor's name removed by <?php the_author(); ?> --></span>
		</div>
		

	</div>

	<?php include (TEMPLATEPATH . '/inc/nav.php' ); ?>

	<?php endwhile; endif; ?>