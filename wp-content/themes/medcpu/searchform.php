<form action="<?php bloginfo('siteurl'); ?>" id="searchform" method="get">
    <div>
        <label for="s" class="screen-reader-text">Search for:</label>
        <input type="text" id="s" name="s" value="Search website" onblur="if(this.value == '') { this.value='Search website'}" onfocus="if (this.value == 'Search website') {this.value=''}" />
    </div>
</form>