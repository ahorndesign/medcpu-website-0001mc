		<div id="footer">
			<div id="footer-nav">
				<div id="footer-search"><?php get_search_form( $echo ); ?></div>
				<?php wp_nav_menu( array('menu' => 'footer-menu')); ?>
			</div>
			<div id="footer-warea">
				<?php if ( !function_exists('dynamic_sidebar')
				|| !dynamic_sidebar('footer-widgets') ) : ?>
				<?php endif; ?>
			</div>
			<div class="clear"></div>
		</div>
	</div>
	<div id="wrap-footer"></div>
</div>

	<?php wp_footer(); ?>
	
	<!-- Don't forget analytics -->
	<script type="text/javascript">

	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-24209534-1']);
	  _gaq.push(['_trackPageview']);

	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();

	</script>
	
</body>

</html>
