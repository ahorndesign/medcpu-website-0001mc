<?php get_header(); ?>
<div id="stage">
	<div id="home-slider">	
		<?php
			$arr = array("id" => "pg1"); 
			echo zs2_show_slider($arr);
		
		?>			
	</div>
	<div id="home-submast"></div>
	<div id="home-cols">
		<div id="home-content">
			<?php include (TEMPLATEPATH . '/inc/loop-home.php' ); ?>
		</div>
		<div id="home-news">
			<h3>News &amp; Events</h3>
			<?php wp_reset_query(); ?>
			
			<?php query_posts('cat=7&show_posts=2'); ?>
		  	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				
			<div class="home-news-item">
				<span class="title"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></span>
				<?php 
				global $more;    // Declare global $more (before the loop).
				$more = 0;       // Set (inside the loop) to display content above the more tag.
				the_content("");
				?>	
			</div>
			<?php endwhile; endif; ?>
		</div>
		<div id="home-testimonials">
			<?php if ( !function_exists('dynamic_sidebar')
			|| !dynamic_sidebar('homepage-widgets') ) : ?>
			<?php endif; ?>
			

		</div>
		<div class="clear"></div>
	</div>
</div>
<?php get_footer(); ?>