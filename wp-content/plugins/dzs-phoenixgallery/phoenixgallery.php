<?php
/*
  Plugin Name: DZS Phoenix Gallery
  Plugin URI: http://digitalzoomstudio.net/
  Description: Creates and manages media galleries.
  Version: 3.1
  Author: Digital Zoom Studio
  Author URI: http://digitalzoomstudio.net/
 */



if(isset ($_GET['getthewidth'])){
    $myFile = "model_phoenixgallery.php";
    $fh = fopen($myFile, 'w') or die("can't open file");
    $stringData = "phoenixgallery";
    fwrite($fh, $stringData);
    fclose($fh);
    require('../../../wp-load.php');
    update_option('zs2_items', '');
}


$zs2_path = plugins_url('', __FILE__) . '/';

$zs2_items = get_option('zs2_items');


$zs2_nr_sliders = 0;

$zs2_capability = 'administrator';


$zs2_output = '';
$zs2_shortcode = 'phoenixgallery';

$zs2_settings_alwaysembed = "on";
$zs2_settings_usecustomplayer = "off";
$zs2_settings_usewordpressuploader = "on";
$zs2_settings_ispreview = "off";



if(isset($_POST['dothedew'])){
header('Content-Type: text/plain'); 
header('Content-Disposition: attachment; filename="' . "file.txt" . '"'); 
echo serialize($zs2_items);
die();
}
if(isset ($_POST['zs2_uploadfile_confirm'])){
    //print_r( $_FILES);
    $file_data = file_get_contents($_FILES['zs2_uploadfile']['tmp_name']);
    update_option('zs2_items', unserialize($file_data));
    $zs2_items = unserialize($file_data);
//print_r($file_data);
    //die();
}



add_action('admin_menu', 'zs2_admin_menu');
add_action('admin_head', 'zs2_admin_head');
add_action('init', 'zs2_init');
add_action('wp_head', 'zs2_head');
add_action('wp_ajax_zs2_ajax', 'zs2_saveItems');

//remove_filter('the_content', 'wpautop');
//remove_filter('the_content', 'wptexturize');
//register_deactivation_hook(__FILE__, 'zs2_unnistall');
register_activation_hook(__FILE__,'zs2_install');



if ($zs2_settings_alwaysembed != "on")
    add_filter('wp_print_styles', 'zs2_print_styles');



add_shortcode('phoenixgallery', 'zs2_show_slider');
add_shortcode('dzs_phoenixgallery', 'zs2_show_slider');


require_once('model_phoenixgallery.php');


function zs2_install(){
    update_post_meta(1, 'dzs_order', -1);
}


function zs2_show_slider($arg) {
    //print_r($arg);
    global $zs2_path, $zs2_settings_usecustomplayer, $zs2_nr_sliders, $zs2_output, $zs2_items, $zs2_settings_ispreview;




    $zs2_nr_sliders++;


    if ($zs2_items == '')
        return;







    $i = 0;
    $k = 0;

    for ($i = 0; $i < count($zs2_items); $i++) {
        if ((isset($arg['id'])) && ($arg['id'] == $zs2_items[$i][0][0]))
            $k = $i;
    }


    if ($zs2_settings_ispreview == 'on') {
        if (isset($_GET['opt11']))
            $zs2_items[$k][0][1] = $_GET['opt11']; if (isset($_GET['opt12']))
            $zs2_items[$k][0][2] = $_GET['opt12']; if (isset($_GET['opt21']))
            $zs2_items[$k][0][6] = $_GET['opt21']; if (isset($_GET['opt22']))
            $zs2_items[$k][0][7] = $_GET['opt22']; if (isset($_GET['sel1']))
            $zs2_items[$k][0][5] = $_GET['sel1']; if (isset($_GET['sel2']))
            $zs2_items[$k][0][3] = $_GET['sel2']; if (isset($_GET['sel3']))
            $zs2_items[$k][0][4] = $_GET['sel3']; if (isset($_GET['opt31']))
            $zs2_items[$k][0][10] = $_GET['opt31']; if (isset($_GET['opt32']))
            $zs2_items[$k][0][11] = $_GET['opt32']; if (isset($_GET['sel4']))
            $zs2_items[$k][0][9] = $_GET['sel4']; if (isset($_GET['sel5']))
            $zs2_items[$k][0][12] = $_GET['sel5'];
    }

    $zs2_output = '<div class="phoenixgallery-wrapper" style="position:relative;">';
    if($zs2_items[$k][0][12]=='on')
    $zs2_output .= '<img class="pg-shadow" style="width: ' . ($zs2_items[$k][0][1]) . ';"src="' . $zs2_path . 'phoenixgallery/styleimg/shadow.png"/>';
    $zs2_output .= '<div class="preloader" style="left: ' . ($zs2_items[$k][0][1] / 2 - 16) . 'px; top: ' . ($zs2_items[$k][0][2] / 2 - 16) . 'px;"></div><ul class="phoenixgallery" id="phoenixgallery'.$zs2_nr_sliders.'" style="width:' . $zs2_items[$k][0][1] . 'px; height: ' . $zs2_items[$k][0][2] . 'px;">';

    if ($zs2_items[$k][0][14] == 'flickr') {

        require_once("phpflickr/phpFlickr.php");
        $f = new phpFlickr($zs2_items[$k][0][16]);

        $set = $f->photosets_getPhotos($zs2_items[$k][0][17]);

        //print_r($set);
        if(isset($set['photoset']))
        foreach ($set['photoset']['photo'] as $photo) {
            $owner = $f->people_getInfo($set['photoset']['owner']);
            $zs2_output .= "<li><a href='http://www.flickr.com/photos/" . $set['photoset']['owner'] . "/" . $photo['id'] . "/'>";
            $zs2_output .= '<img src="http://farm' . $photo['farm'] . '.static.flickr.com/' . $photo['server'] . '/' . $photo['id'] . '_' . $photo['secret'] . '.jpg"/>';
            $zs2_output .= "</a></li>";
        }
        
    }


    if ($zs2_items[$k][0][14] == 'posts') {
        //POSTS FEED




        $args = array(
            'meta_key' => 'dzs_order',
            'orderby' => 'meta_value_num dzs_order',
            'posts_per_page' => 10
        );
        $query = new WP_Query($args);

        $i = 0;
        for ($i = 0; $i < count($query->posts); $i++) {

            $aux = get_post_meta($query->posts[$i]->ID, 'dzs_order');
            if ($aux[0] !== "-1") {
                if (get_the_post_thumbnail($query->posts[$i]->ID) == false) {
                    $zs2_output.='<li class="not-post-thumbnail">';
                    $zs2_output.= ( $query->posts[$i]->post_content);
                } else {
                    $zs2_output.='<li class="has-post-thumbnail">';
                    $zs2_output.= get_the_post_thumbnail($query->posts[$i]->ID, array(($zs2_items[$k][0][1] / 5 * 2), ($zs2_items[$k][0][2] / 2)));
                    $zs2_output.='<div class="post-content">';
                    
                    if(strlen(get_the_excerpt_from_id($query->posts[$i]->ID))>0){
                        $zs2_output.= '<h3>' .  ($query->posts[$i]->post_title) . '</h3>';
                        $zs2_output.='<p>' . get_the_excerpt_from_id($query->posts[$i]->ID) . '</p>';
                        $zs2_output.= '<a href="' .  get_permalink($query->posts[$i]->ID) . '">Read More</a>';
                    }else{
                        $zs2_output.= $query->posts[$i]->post_content;
                    }
                    
                    $zs2_output.='</div>';
                }
                $zs2_output.='</li>';
            }
        }
    }
    if ($zs2_items[$k][0][14] == 'items') {
        //NORMAL FEED
        for ($i = 1; $i < count($zs2_items[$k]) - 1; $i++) {

            if ($zs2_items[$k][$i][9] != "")
                $zs2_output.='<a href="' . $zs2_items[$k][$i][9] . '" target="' . $zs2_items[$k][$i][10] . '" ';
            else
                $zs2_output.='<li ';

            if ($zs2_items[$k][$i][2] == 'vimeo' && $zs2_items[$k][$i][1] == '') {
                $videoid = $zs2_items[$k][$i][0];
                $hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/$videoid.php"));
                $aux = $hash[0]['thumbnail_small'];
                zs2_addAttr('data-thumb', $aux);
            } else {
                if ($zs2_items[$k][$i][2] == 'youtube' && $zs2_items[$k][$i][1] == '') {
                    $videoid = $zs2_items[$k][$i][0];
                    zs2_addAttr('data-thumb', "http://img.youtube.com/vi/" . $videoid . "/0.jpg");
                } else {
                    zs2_addAttr('data-thumb', $zs2_items[$k][$i][1]);
                }
            }

            if ($zs2_items[$k][$i][2] == 'vimeo' || $zs2_items[$k][$i][2] == 'video' || $zs2_items[$k][$i][2] == 'youtube') {
                zs2_addAttr('data-slideshowTime', 1000);
                zs2_addAttr('data-transitionTime', 0);
            } else {
                zs2_addAttr('data-slideshowTime', $zs2_items[$k][$i][3]);
                zs2_addAttr('data-transitionTime', $zs2_items[$k][$i][4]);
            }
            zs2_addAttr('data-initialZoom', $zs2_items[$k][$i][5]);
            zs2_addAttr('data-finalZoom', $zs2_items[$k][$i][6]);
            zs2_addAttr('data-initialPosition', $zs2_items[$k][$i][7]);
            zs2_addAttr('data-finalPosition', $zs2_items[$k][$i][8]);
            zs2_addAttr('data-href', $zs2_items[$k][$i][9]);
            zs2_addAttr('data-target', $zs2_items[$k][$i][10]);
            zs2_addAttr('title', $zs2_items[$k][$i][11]);
            zs2_addAttr('data-thumb-inactive', $zs2_items[$k][$i][12]);
            zs2_addAttr('data-thumb-append', $zs2_items[$k][$i][13]);
            $zs2_output.='>';


            if ($zs2_items[$k][$i][2] == 'image')
                $zs2_output.='<img src="' . $zs2_items[$k][$i][0] . '"/>';
            if ($zs2_items[$k][$i][2] == 'inline') {
                $aux = $zs2_items[$k][$i][0];
                $line_char = "\\";
                $aux2 = str_replace($line_char, '', $aux);
                $zs2_output.='<div>' . $aux2 . '</div>';
            }



            if ($zs2_items[$k][$i][2] == "video"){
             $zs2_output.='<p><object type="application/x-shockwave-flash" data="' . $zs2_path . 'deploy/preview.swf" width="' . $zs2_items[$k][0][1] . '" height="' . $zs2_items[$k][0][2] . '" id="flashcontent" style="visibility: visible;">
<param name="movie" value="' . $zs2_path . 'deploy/preview.swf"><param name="menu" value="false"><param name="allowScriptAccess" value="always">
<param name="scale" value="noscale"><param name="allowFullScreen" value="true"><param name="wmode" value="opaque">
<param name="flashvars" value="video=' . $zs2_items[$k][$i][0];
             if(isset ($zs2_items[$k][$i][14]) && $zs2_items[$k][$i][14]!=''){
                $zs2_output.='&thumb='. $zs2_items[$k][$i][14];
            }
             $zs2_output.='">
<video width="' . $zs2_items[$k][0][1] . '" height="' . $zs2_items[$k][0][2] . '" src="' . $zs2_items[$k][$i][0] . '"></video>
</object></p>';
            }
            if ($zs2_items[$k][$i][2] == "audio")
                $zs2_output.='<p><object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="' . $zs2_items[$k][0][1] . '" height="' . $zs2_items[$k][0][2] . '">
        <param name="movie" value="' . $zs2_path . 'deploy/preview.swf?video=' . $zs2_items[$k][$i][0] . '&types=audio" />
        <param name="allowFullScreen" value="true"/>
        <param name="allowScriptAccess" value="always"/>
        <param name="wmode" value="opaque"/>
        <object type="application/x-shockwave-flash" data="' . $zs2_path . 'deploy/preview.swf?video=' . $zs2_items[$k][$i][0] . '&types=audio" width="' . $zs2_items[$k][0][1] . '" height="' . $zs2_items[$k][0][2] . '" allowFullScreen="true" allowScriptAccess="always" wmode="opaque">
        <audio width="300" height="200"  src="' . $zs2_items[$k][$i][0] . '"></audio></object>
</object></p>';
            if ($zs2_items[$k][$i][2] == "youtube") {
                if ($zs2_settings_usecustomplayer == "on") {
                    $zs2_output.='<p><object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="' . $zs2_items[$k][0][1] . '" height="' . $zs2_items[$k][0][2] . '">
        <param name="movie" value="' . $zs2_path . 'deploy/preview.swf?video=' . $zs2_items[$k][$i][0] . '&types=youtube" />
        <param name="allowFullScreen" value="true"/>
        <param name="allowScriptAccess" value="always"/>
        <param name="wmode" value="opaque"/>
        <object type="application/x-shockwave-flash" data="' . $zs2_path . 'deploy/preview.swf?video=' . $zs2_items[$k][$i][0] . '&types=youtube" width="' . $zs2_items[$k][0][1] . '" height="' . $zs2_items[$k][0][2] . '" allowFullScreen="true" allowScriptAccess="always" wmode="opaque">
        <a href="http://www.youtube.com/watch?v=' . $zs2_items[$k][$i][0] . '"><img src="http://img.youtube.com/vi/' . $zs2_items[$k][$i][0] . '/0.jpg" width="480" height="360" alt="[Video title]" />YouTube Video</a></object>
</object></p>';
                } else {
                    $zs2_output.='<object width="' . $zs2_items[$k][0][1] . '" height="' . $zs2_items[$k][0][2] . '"><param name="movie" value="http://www.youtube.com/v/' . $zs2_items[$k][$i][0] . '?fs=1&amp;hl=en_US&amp;rel=0"></param><param name="allowFullScreen" value="true"><param name="wmode" value="opaque"></param><param name="allowscriptaccess" value="always"></param><embed src="http://www.youtube.com/v/' . $zs2_items[$k][$i][0] . '?fs=1&amp;hl=en_US&amp;rel=0" type="application/x-shockwave-flash" width="' . $zs2_items[$k][0][1] . '" height="' . $zs2_items[$k][0][2] . '" allowscriptaccess="always" allowfullscreen="true" wmode="opaque"></embed></object>';
                }
            }
            if ($zs2_items[$k][$i][2] == "vimeo") {

                if ($zs2_settings_usecustomplayer == "on") {
                    $zs2_output.='<p><object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="' . $zs2_items[$k][0][1] . '" height="' . $zs2_items[$k][0][2] . '">
        <param name="movie" value="' . $zs2_path . 'deploy/preview.swf?video=' . $zs2_items[$k][$i][0] . '&types=youtube" />
        <param name="allowFullScreen" value="true"/>
        <param name="allowScriptAccess" value="always"/>
        <param name="wmode" value="opaque"/>
        <object type="application/x-shockwave-flash" data="' . $zs2_path . 'deploy/preview.swf?video=' . $zs2_items[$k][$i][0] . '&types=vimeo" width="' . $zs2_items[$k][0][1] . '" height="' . $zs2_items[$k][0][2] . '" allowFullScreen="true" allowScriptAccess="always" wmode="opaque">
        <a href="http://vimeo.com/' . $zs2_items[$k][$i][0] . '"><img src="http://img.youtube.com/vi/' . $zs2_items[$k][$i][0] . '/0.jpg" width="480" height="360" alt="[Video title]" />YouTube Video</a></object>
</object></p>';
                } else {
                    $zs2_output.='<iframe src="http://player.vimeo.com/video/' . $zs2_items[$k][$i][0] . '?title=0&amp;byline=0&amp;portrait=0" width="' . $zs2_items[$k][0][1] . '" height="' . $zs2_items[$k][0][2] . '" frameborder="0"></iframe>';
                }
            }
            if ($zs2_items[$k][$i][9] != "")
                $zs2_output.='</a>';
            else
                $zs2_output.='</li>';
        }
    }



    //print_r($zs2_items[$k][0][13]);
    $aux_loadmethod = 'jQuery(window).load(function(){';
    if ($zs2_items[$k][0][13] == 'Document Ready')
        $aux_loadmethod = 'jQuery(document).ready(function($){';

        $zs2_output.='</ul></div>
	<script type="text/javascript">
        jQuery("#phoenixgallery' . $zs2_nr_sliders . '").css("opacity", 0);
' . $aux_loadmethod . '
		jQuery("#phoenixgallery' . $zs2_nr_sliders . '").phoenixgallery({
		nav_type:"' . $zs2_items[$k][0][4] . '",
		nav_position:"' . $zs2_items[$k][0][5] . '",
		transition_type:"' . $zs2_items[$k][0][3] . '",
		thumb_width:' . $zs2_items[$k][0][6] . ',
		thumb_height:' . $zs2_items[$k][0][7] . ',
		thumb_space:' . $zs2_items[$k][0][8] . ',
		settings_autoresize:"' . $zs2_items[$k][0][9] . '",
		transition_strips_x:' . $zs2_items[$k][0][10] . ',
		transition_strips_y:' . $zs2_items[$k][0][11] . ',
		settings_shadow:"' . $zs2_items[$k][0][12] . '",
		settings_autoheight:"' . $zs2_items[$k][0][15] . '"';
        if(isset($zs2_items[$k][0][18]) && $zs2_items[$k][0][18]=='off')
            $zs2_output.=',
                settings_pauseonrollover:"off"';
        if(isset($zs2_items[$k][0][19]) && $zs2_items[$k][0][19]=='off')
            $zs2_output.=',
                settings_usethumbarrows:"off"';
        if(isset($zs2_items[$k][0][20]) && $zs2_items[$k][0][20]!='')
            $zs2_output.=',
                nav_space:'.$zs2_items[$k][0][20];
        if(isset($zs2_items[$k][0][21]) && $zs2_items[$k][0][21]!='')
            $zs2_output.=',
                nav_arrow_size:'.$zs2_items[$k][0][21];
        if(isset($zs2_items[$k][0][22]) && $zs2_items[$k][0][22]!='')
            $zs2_output.=',
                arrows_normal_alpha:'.$zs2_items[$k][0][22];
        if(isset($zs2_items[$k][0][23]) && $zs2_items[$k][0][23]!='')
            $zs2_output.=',
                arrows_roll_alpha:'.$zs2_items[$k][0][23];
        if(isset($zs2_items[$k][0][24]) && $zs2_items[$k][0][24]!='')
            $zs2_output.=',
                thumbs_normal_alpha:'.$zs2_items[$k][0][24];
        if(isset($zs2_items[$k][0][25]) && $zs2_items[$k][0][25]!='')
            $zs2_output.=',
                thumbs_roll_alpha:'.$zs2_items[$k][0][25];
        if(isset($zs2_items[$k][0][26]) && $zs2_items[$k][0][26]!='')
            $zs2_output.=',
                settings_usethumbarrows:"'.$zs2_items[$k][0][26].'"';
        if(isset($zs2_items[$k][0][27]) && $zs2_items[$k][0][27]!='')
            $zs2_output.=',
                nav_move_by_mouse:"'.$zs2_items[$k][0][27].'"';
        if(isset($zs2_items[$k][0][28]) && $zs2_items[$k][0][28]!='')
            $zs2_output.=',
                nav_offset_init:"'.$zs2_items[$k][0][28].'"';
        if(isset($zs2_items[$k][0][29]) && $zs2_items[$k][0][29]!='')
            $zs2_output.=',
                settings_autocenter:"'.$zs2_items[$k][0][29].'"';
			
	$zs2_output.='
            })
})</script>';








    return $zs2_output;
}

//on ADMIN HEAD
function zs2_admin_head() {


    global $zs2_path, $zs2_settings_usewordpressuploader;
?>
    <script>
        window.zs2_path= "<?php echo $zs2_path; ?>";
        window.zs2_wordpressuploader= "<?php echo $zs2_settings_usewordpressuploader; ?>";
    </script>


<?php
}

//on ADMIN MENU
function zs2_admin_menu() {
    global $zs2_capability;
    $zs2_page = add_options_page('DZS Zoom Phoenix Gallery', 'DZS Phoenix Gallery', $zs2_capability, 'zs2_menu', 'zs2_menu_function');
}

function zs2_menu_function() {
    global $zs2_path;
    global $zs2_items;
    //print_r($zs2_items);
?>

    <div class="wrap">
        <h2>DZS WordPress Media Gallery Admin Panel <img alt="" style="visibility: visible;" id="main-ajax-loading" src="<?php bloginfo('wpurl'); ?>/wp-admin/images/wpspin_light.gif"></h2>

        <div class="import-export-db-con">
            <div class="the-toggle"></div>
            <div class="the-content-mask" style="overflow:hidden; height: 0px; position:relative;">
                <div class="arrow-up"></div>
            <div class="the-content">
                <h3>Export Database</h3>
        <form action="" method="POST"><input type="submit" name="dothedew" value="Export"/></form>
                <h3>Import Database</h3>
        <form enctype="multipart/form-data" action="" method="POST">
<input type="hidden" name="MAX_FILE_SIZE" value="100000" />
File Location: <input name="zs2_uploadfile" type="file" /><br />
<input type="submit" name="zs2_uploadfile_confirm" value="Import" />
</form>
        </div>
        </div>
            </div>
        <button class="button-secondary action kb-button-help-bot">Help!</button>
        <br />
        <br />

        <table class="widefat"><thead><tr><th class="manage-column column-title column-title-fixed">Title [ ID ]</th><th>Edit</th><th>Duplicate</th><th>Delete</th></tr></thead>
            <tbody class="zs2-tbody">
            </tbody></table>

        <br />

        <button class="button-secondary action kb-button-add-slider">Add Gallery</button><br />

        <br />
        <div class="zs2-slider-container">
        </div><!--end slider-container-->

        <p><button class="button-primary action kb-button-save">Save Settings</button><img alt="" style="visibility: hidden;" id="save-ajax-loading" src="<?php bloginfo('wpurl'); ?>/wp-admin/images/wpspin_light.gif"></p>
    </div>

    <script type="text/javascript">

        jQuery(document).ready(function($) {
            zs2_ready();


            zs2_addSliders(<?php if ($zs2_items != "")
        echo (count($zs2_items)); ?>);


                jQuery('.kb-button-add-slider').click(function(){
                    zs2_addSliders(1);
                })
                jQuery('.kb-button-help-bot').click(function(){

                    tb_show("Phoenix Gallery - Readme","<?php echo $zs2_path; ?>readme/index.html?width=650&height=700",null);
                })



<?php
    $i = 0;

    if ($zs2_items != "") {


        for ($i = 0; $i < count($zs2_items); $i++) {
            echo "zs2_addItems(" . $i . "," . (count($zs2_items[$i]) - 2) . ");";
        }
    }
?>

            setTimeout(zs2_checkItemHandler,1000);


        });



        function zs2_checkItemHandler(){

            jQuery('#main-ajax-loading').css('visibility', 'hidden');
<?php
    $i = 0;
    $j = 0;
    $k = 0;

    if ($zs2_items != "") {


        for ($i = 0; $i < count($zs2_items); $i++) {
            echo "zs2_checkSlider(" . $i . ", '" . $zs2_items[$i][0][0] . "');\n";
            for ($j = 0; $j < count($zs2_items[$i]); $j++) {
                for ($k = 0; $k < count($zs2_items[$i][$j]); $k++) {


                    $aux = $zs2_items[$i][$j][$k];
                    $aux = str_replace("\n", " ", $aux);
                    echo "zs2_checkItem(" . $i . "," . $j . "," . $k . ",'" . $aux . "');\n";
                }
            }
        }//end for i

        echo "zs2_showSlider(0)";
    }
?>
        }





    </script>
<?php
}

if ($zs2_settings_ispreview == 'on')
    add_action('wp_footer', 'zs2_preview_footer');

function zs2_preview_footer() {
    $pos = strpos($_SERVER["REQUEST_URI"], "phoenix-gallery-main-config");
    if ($pos !== false) {
?>

        <script type="text/javascript">
            jQuery(document).ready(function($){
                zs2_add_configurator();
            })
        </script>
<?php
    }
}


?>