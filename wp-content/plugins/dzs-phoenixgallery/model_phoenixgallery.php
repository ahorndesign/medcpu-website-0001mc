<?php

function zs2_print_styles() {
    global $zs2_path, $zs2_shortcode, $post;

    wp_enqueue_script('jquery');


    $pos = false;
    if ($post)
        $pos = strpos($post->post_content, '[' . $zs2_shortcode);


    if ($pos !== false) {
        zs2_enqueue_scripts();
    }
}

function zs2_enqueue_scripts() {
    global $zs2_path;
    wp_enqueue_script('phoenixgallery', $zs2_path . "phoenixgallery/phoenixgallery.js");
    wp_enqueue_style('phoenixgallery_style', $zs2_path . 'phoenixgallery/style.css');
}

function shorten_string($arg, $limit)
{
  return substr($arg, 0, $limit);
}

function get_the_excerpt_from_id($arg)
{
  global $wpdb;

  $dbquery = "SELECT post_excerpt FROM wp_posts WHERE ID = " . $arg . " LIMIT 1";
  $result = $wpdb->get_results($dbquery);
  return $result[0]->post_excerpt;
}

function zs2_head() {
    global $zs2_path;
}

function zs2_init() {
    global $zs2_path, $zs2_settings_alwaysembed, $zs2_settings_usewordpressuploader, $zs2_settings_ispreview;

    wp_enqueue_script('jquery');

    if (is_admin ()) {
        if (isset($_REQUEST['page']) && $_REQUEST['page'] == "zs2_menu") {

            if ($zs2_settings_usewordpressuploader == 'on') {
                wp_enqueue_script('media-upload');
                wp_enqueue_script('tiny_mce');
                wp_enqueue_script('thickbox');
                wp_enqueue_style('thickbox');
            } else {
                wp_enqueue_script('dzs-upload', $zs2_path . 'uploader/js/dzs_upload.js');
                wp_enqueue_script('dzs-upload-form', $zs2_path . 'uploader/js/jquery.form.js');
                wp_enqueue_style('dzs-upload', $zs2_path . 'uploader/style/upload.css');
            }
            
            //echo $zs2_path;
            wp_enqueue_script('zs2-admin', $zs2_path . 'js/admin.js');
            wp_enqueue_style('zs2-admin-style', $zs2_path . "style/admindefault/style.css");

            wp_register_style('jquery.ui.all', $zs2_path . "style/base/jquery.ui.all.css");
            wp_enqueue_style('jquery.ui.all');

            wp_enqueue_script('jquery-ui-core');
            wp_enqueue_script('jquery-ui-sortable');
            
            
            wp_enqueue_style('itoggle', $zs2_path . 'itoggle/style.css');
            wp_enqueue_script('itoggle', $zs2_path . 'itoggle/iphone-style-checkboxes.js');
        }
    } else {
        if ($zs2_settings_alwaysembed == "on")
            zs2_enqueue_scripts();
        
                if ($zs2_settings_ispreview == 'on')
                wp_enqueue_script('dzs-upload', $zs2_path . 'js/configurator.js');
    }
}

function zs2_addAttr($arg1, $arg2) {
    global $zs2_output;
    //$arg2 = str_replace('\\', '', $arg2);
    if (isset ($arg2)  && $arg2 != "undefined" && $arg2 != '')
        $zs2_output.= $arg1 . "='" . $arg2 . "' ";
}

function zs2_saveItems() {
    global $wpdb; // this is how you get access to the database

    $i = 0;
    $j = 0;
    $k = 0;


    $mainOptionsArray = array();
    $secondaryOptionsArray = array();

    $arrayTemp = $_POST['arrayMain'];
    $arraySliders = explode('&.', $arrayTemp);



    for ($i = 0; $i < count($arraySliders); $i++) {
        $secondaryOptionsArray = array();
        $arrayItemsAux = explode('&;', $arraySliders[$i]);

        for ($j = 0; $j < count($arrayItemsAux); $j++) {
            $arraySettingsAux = explode('&,', $arrayItemsAux[$j]);
            array_push($secondaryOptionsArray, $arraySettingsAux);
        }//end j

        array_push($mainOptionsArray, $secondaryOptionsArray);
    }//end i
    //print_r($mainOptionsArray);

    if ($mainOptionsArray[0][0][0] == '')
        update_option('zs2_items', '');
    else
        update_option('zs2_items', $mainOptionsArray);


    die();
}

require_once('widget_phoenixgallery.php');

function zs2_unnistall() {
    update_option('zs2_items', '');
}
?>