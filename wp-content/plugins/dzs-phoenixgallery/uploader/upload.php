<?php

/*
 * DZS Upload
 * version: 1.0
 * author: digitalzoomstudio
 * website: http://digitalzoomstudio.net
 *
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 */


$path= "upload/".$HTTP_POST_FILES['file_field']['name'];
if(isset($HTTP_POST_FILES['file_field']['tmp_name'])){
    $file_name =  'file name: ' . $HTTP_POST_FILES['file_field']['name'];
    $pos = strpos($file_name, ".php");
    //echo $pos;
    if ($pos !== false){
        echo "You cannot upload php files.";
        die();
    }

}

if(copy($HTTP_POST_FILES['file_field']['tmp_name'], $path))
{
echo "Success";
}
else
{
echo "Error";
}

?>