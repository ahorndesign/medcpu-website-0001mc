/*
 * DZS Upload
 * version: 1.0
 * author: digitalzoomstudio
 * website: http://digitalzoomstudio.net
 *
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 */
window.dzs_upload_target = "";
var target_field;

jQuery(document).ready(function($) {
	//Firefox 4, Chrome, Safari - only select photo button
	//Opera, IE9, IE8 - only browse button
	//IE7 - browser & submit
	if(jQuery.browser.opera || jQuery.browser.msie){
		jQuery('.dzs-upload').children('.btn_upl').css('display','none');
		jQuery('.dzs-upload').children('.file_field').css('visibility','visible');
	}
	
	dzs_initUpload();
}); 
function dzs_initUpload(){
	//console.log(window.zs2_path);
	var uploadPath = "";
	uploadPath = window.zs2_path + 'uploader/upload/'
            var options = {
            target: '#message', //Div tag where content info will be loaded in
            url:window.zs2_path + 'uploader/upload.php', //The php file that handles the file that is uploaded
            beforeSubmit: function() {
				//console.log('ceva');
            },
            success:  function() {
                //Here code can be included that needs to be performed if Ajax request was successful
                //alert('uploaded');
                
            }
            };
            
           
            jQuery('.dzs-upload').unbind(); jQuery('.dzs-upload').submit(function() {
                jQuery(this).ajaxSubmit(options);
                return false;
            });
			
            
            jQuery('.file_field').unbind(); jQuery('.file_field').change(function() {
            	target_field = jQuery(this).parent().prev();
            	var aux = jQuery(this).val();
            	if(aux.indexOf('/')>-1)
            	aux = aux.split('/');
            	else
            	aux = aux.split('\\');
            	
            	auxfinal=aux[aux.length-1];
            	//console.log(aux);
               window.dzs_upload_target= auxfinal;
               
               if(target_field.attr('type')=='text' || target_field.hasClass('textinput'))
               target_field.val(uploadPath + auxfinal);
               //console.log(window.dzs_upload_target)
               jQuery(this).parent().submit();
            });

            
            jQuery('.btn_upl').unbind(); jQuery('.btn_upl').click(function() {
            jQuery(this).next().click();
			});
				
}
