
var zs2_adm$cacheem='';
var zs2_adminSlider='';
var zs2_adminTableSlider='';
var zs2_nrItems=0;
var zs2_nrSliders=0;
var zs2_cacheNr=0;
var zs2_cacheObject;
var i=0;
var zs2_targetInput;
var zs2_targetImg;

var zs2_currSlider;
var zs2_currSliderNr=-1;//curr slider index
var zs2_currItem;

var cauta='';
//cauta = 'http://localhost/cribn.json';
//<div class="setting"><div class="setting_label">XML Name:</div><input type="text" class="short textinput zs2-input-id" value="gallery' + parseInt(Math.random() * 999) + '.xml"/><span class="sidenote">Must be unique</span></div>\
zs2_adminTableSlider = '<tr class="zs2-table-row"><th class="manage-column column-title column-title-fixed"><span class="zs2-table-row-slider-id">default</span></th><th class="zs2-edit-slider">Edit Gallery</th><th class="zs-duplicate-slider">Duplicate Gallery</th><th class="zs2-delete-slider">Delete Gallery</th></tr>';

var zs2_arraySliders = [];//array that holds nr items in each slider




function zs2_itemExpand(){


    zs2_cacheNr = zs2_currSlider.find('.item_header').index(jQuery(this)) + 1;

    if (jQuery(this)[0].nodeName == "IMG") {
        zs2_cacheNr = zs2_currSlider.find('.preview-img').index(jQuery(this)) + 1;
        window.scrollTo(0, zs2_currSlider.find('.settings_cont').eq(zs2_cacheNr).parent().offset().top)

    }

    if (zs2_cacheNr == 0)
        return;


    if (zs2_currSlider.find('.settings_cont').eq(zs2_cacheNr).css('display') != 'none')
        zs2_currSlider.find('.settings_cont').eq(zs2_cacheNr).hide('slow');
    else
        zs2_currSlider.find('.settings_cont').eq(zs2_cacheNr).show('slow');


}
function zs2_ready(){
    jQuery('#main-ajax-loading').css('visibility', 'hidden');
    jQuery('.kb-button-save').click(zs2_presendGallery);
	jQuery('.import-export-db-con .the-toggle').click(function(){
		var $t = jQuery(this);
		var $cont = $t.parent().children('.the-content-mask');
		/*
		if($cont.css('display')=='none')
		$cont.slideDown('slow');
		else
		$cont.slideUp('slow');
		*/
		if($cont.css('height')=='0px')
		$cont.stop().animate({
			'height' : 400
		}, 700);
		else
		$cont.stop().animate({
			'height' : 0
		}, 700);
		
	});
}
function zs2_makeButtons(){
	
	
	jQuery('.item_header').unbind();
	jQuery('.item_header').click(zs2_itemExpand)
	
	jQuery('.upload_file').unbind();
	jQuery('.upload_file').click(zs2_uploadMedia);

	jQuery('.button-delete').unbind();
	jQuery('.button-delete').click(zs2_deleteItem);



	jQuery('.preview-img').unbind();
	jQuery('.preview-img').click(zs2_itemExpand)

	jQuery('.btn-img-delete').unbind();
	jQuery('.btn-img-delete').click(zs2_deleteItem);
	
	
	jQuery('.zs-duplicate-slider').unbind();
	jQuery('.zs-duplicate-slider').click(zs_duplicate_slider);


	jQuery('.zs2-delete-slider').unbind();
	jQuery('.zs2-delete-slider').click(zs2_deleteSliderHandler);

	jQuery('.zs2-edit-slider').unbind();
	jQuery('.zs2-edit-slider').click(function(){zs2_enableSlider(jQuery('.zs2-edit-slider').index(jQuery(this)));})
	
	
	jQuery('.zs2-type-select').unbind();
    jQuery('.zs2-type-select').change(zs2_handle_select);
	
	
	//jQuery('.zs2-file-input').unbind();
	//jQuery('.zs2-file-input').change(function(){ zs2_changePreview(jQuery(this))});
	
	
	
	jQuery('.toggle-title').unbind();
	jQuery('.toggle-title').bind('click', function(){
		var $t = jQuery(this);
		if($t.hasClass('opened')){
			($t.parent().find('.toggle-content').slideUp('fast'));
			$t.removeClass('opened');
		}else{
			($t.parent().find('.toggle-content').slideDown('fast'));
			$t.addClass('opened');
		}
	})
	
	
	
	for(i=0;i<jQuery('.zs2-slider').length;i++){
	jQuery('.zs2-slider').eq(i).find('.textinput').eq(0).unbind();
	jQuery('.zs2-slider').eq(i).find('.textinput').eq(0).change(function(){zs2_checkSlider(jQuery('.zs2-slider').index(jQuery(this).parent().parent().parent().parent()),jQuery(this).val());})
	}
	
	
	
	jQuery('.kb-button-add-bot').unbind();
	jQuery('.kb-button-add-bot').click(function(){
		zs2_addItems(zs2_currSliderNr, 1);
	})
	
	if(typeof dzs_initUpload == 'function')
	dzs_initUpload();
	jQuery('.iphoneit').iphoneStyle({
		resizeHandle : true,
		resizeContainer:true
	});
	
	/*

*/

	setTimeout(zs2_makeSortable,1000);
}


function zs_duplicate_slider(){
	var ind = (jQuery('.zs-duplicate-slider').index(jQuery(this)))
	jQuery('.zs2-slider-container').append(jQuery('.zs2-slider').eq(ind).clone());
	//console.log(jQuery('.zs-slider').last())
	jQuery('.zs2-slider').last().hide();
	
	
	jQuery('.zs2-tbody').append(zs2_adminTableSlider);
	zs2_makeButtons();
}



function zs2_changePreview(argslider, arg, value) {
	//console.log(argslider, arg, value)
	//console.log(jQuery('.zs2-slider').eq(argslider))
    jQuery('.zs2-slider').eq(argslider).find('.preview-img').eq(arg).attr('src', value);
}

function zs2_changeTitle(argslider, arg, value) {
    jQuery('.zs2-slider').eq(argslider).find('.item_header h3').eq(arg).text(value);
    //jQuery('.zs2-slider').eq(argslider).find('.preview-img').eq(arg).attr('src', value);
}

function zs2_change_source(argslider, arg, value) {
    jQuery('.zs2-slider').eq(argslider).find('textarea').eq(arg).css('height', value)
    
    if(value==80)
    jQuery('.zs2-slider').eq(argslider).find('.item').eq(arg + 1).find('button').eq(0).hide();
    else
    jQuery('.zs2-slider').eq(argslider).find('.item').eq(arg + 1).find('button').eq(0).show();
    
}

function zs2_showSlider(arg){
	if(zs2_currSliderNr>=0){
		jQuery('.zs2-slider').eq(zs2_currSliderNr).hide("slow");
	}
	jQuery('.zs2-slider').eq(arg).show("slow");
	
	zs2_currSliderNr=arg;//zs2-type-select
	zs2_currSlider=jQuery('.zs2-slider').eq(arg);
}

function zs2_enableSlider(arg){
	zs2_showSlider(arg);
}
function zs2_removeIt(){
	jQuery(this).remove();
}
function zs2_deleteItem(){
	var index = jQuery(this).parent().parent().children().index(jQuery(this).parent());
	zs2_currSlider.find('.item').eq(index + 1).remove();

    jQuery(this).parent().remove();
}



function zs2_uploadMedia(){
    zs2_targetInput = jQuery(this).prev();
    
    
    //console.log(window.zs2_wordpressuploader);
    

    var temp = zs2_currSlider.find('.zs2-main-upload').index(jQuery(this));
    

	if(window.zs2_wordpressuploader=='on'){
	//edCanvas='ceva';
	tb_show('', 'media-upload.php?type=image&amp;TB_iframe=true&amp;width=640&amp;height=105');
	}else{
		
	}


	window.send_to_editor = function (arg) {
	    var fullpath = arg;
	    var fullpathArray = fullpath.split('>');
	    //fullpath = fullpathArray[1] + '>';


	    var aux1 = jQuery('.zs2-slider').index(zs2_targetInput.parent().parent().parent().parent().parent().parent());
	    var aux2 = zs2_currSlider.find('.columns-1-1').index(zs2_targetInput.parent().parent());
	    var aux3 = jQuery(fullpath).attr('href');
        var aux4=zs2_targetInput.hasClass('zs2-thumb-main');

        if(aux4==true)
	    zs2_changePreview(aux1, aux2,aux3);

	    zs2_targetInput.val(aux3);

	    tb_remove();

	    zs2_changePreview(zs2_targetInput);
	}



return false;

	
}
	
function zs2_handle_select(){
	var arg=zs2_currSlider.find('.item').index(jQuery(this).parent().parent().parent().parent());
	if(jQuery(this).val()=='inline')
	zs2_change_source(zs2_currSliderNr, arg-1, 80)
	else
	zs2_change_source(zs2_currSliderNr, arg-1, 30)
	
}
function zs2_addSliders(arg){
	
	for(i=0;i<arg;i++){
	
	
	
	jQuery('.zs2-slider-container').append(zs2_adminSlider);
	jQuery('.zs2-tbody').append(zs2_adminTableSlider);
	
	zs2_arraySliders[zs2_nrSliders]=[];
	zs2_arraySliders[zs2_nrSliders][0]=0;// this is the number of current items of the current slider
	zs2_nrSliders++;
	}
	
	zs2_currSlider=jQuery('.zs2-slider').eq(0);
	zs2_makeButtons();
	if(zs2_nrSliders==0)
	zs2_showSlider(zs2_nrSliders-1);
	
}
function zs2_checkSlider(argslider, argvalue){
	jQuery('.zs2-table-row-slider-id').eq(argslider).text(argvalue);
}
function zs2_deleteSliderHandler(){
	zs2_deleteSlider(jQuery('.zs2-delete-slider').index(jQuery(this)));
}
function zs2_deleteSlider(argslider){
	zs2_arraySliders[argslider]=0;
	zs2_nrSliders--;
	
	jQuery('.zs2-slider').eq(argslider).remove();
	jQuery('.zs2-table-row').eq(argslider).remove();
}


function zs2_addItems(argslider, arg, top){
	
	
    
    var upload_button_string='<button class="button-secondary action upload_file zs2-main-upload">Upload</button>';
    
    if(window.zs2_wordpressuploader!='on'){
    	upload_button_string='<form name="upload" class="dzs-upload" action="#" method="POST" enctype="multipart/form-data">\
    	<input type="button" value="Upload" class="btn_upl button-secondary"/>\
        <input type="file" name="file_field" class="file_field"/>\
        <input type="submit" class="btn_submit"/>\
</form>';
    }
    
	
	for(i=0;i<arg;i++){
	adminItem='<div class="item"><div class="item_header"><h3>ITEM ' + (zs2_arraySliders[argslider][0] + 1) + '</h3>\
		<div class="arrow-down"></div></div>\
		<div class="settings_cont" style="display:none">\
		<div class="columns-1-1">\
        <div class="sidenote">Below you will enter your video address. If it is a video from YouTube or Vimeo you just need to enter the id of the video in the "Video:" field. The ID is the bolded part http://www.youtube.com/watch?v=<strong>j_w4Bi0sq_w</strong>. If it is a local video you just need to write its location there or upload it through the Upload button ( .flv format ).</div>\
		<div class="setting"><div class="setting_label">Source:</div><textarea type="text" id="text1" class="textinput long zs2-file-input zs2-source-main zs2-thumb-main" name="firstname" value="">' + window.zs2_path +  'img/def1.jpg</textarea>' + upload_button_string + '</div>\
        <div class="setting"><div class="setting_label">Thumbnail:</div><input type="text" id="text1" class="textinput long zs2-file-input" name="firstname" value=""/>' + upload_button_string + '</div>\
		<div class="setting"><div class="setting_label">Type:</div>\
		<select class="textinput zs2-type-select"><option>image</option><option>youtube</option><option>video</option><option>vimeo</option><option>audio</option><option>inline</option></select></div>\
		<h4>TIME</h4>\
		<div class="setting"><div class="setting_label">Slideshow:</div>\
		<input type="text" id="text1" class="textinput short" name="firstname" value="8"/></div>\
		<div class="setting"><div class="setting_label">Effect:</div>\
		<input type="text" id="text1" class="textinput short" name="firstname" value="10"/></div>\
		<h4>ZOOM</h4>\
		<div class="setting"><div class="setting_label">Initial:</div>\
		<input type="text" id="text1" class="textinput short" name="firstname" value="1"/></div>\
		<div class="setting"><div class="setting_label">Final:</div>\
		<input type="text" id="text1" class="textinput short" name="firstname" value="1"/></div>\
		<h4>POSITION</h4>\
		<div class="setting"><div class="setting_label">Initial:</div>\
		<select class="textinput"><option>topLeft</option><option>topCenter</option><option>topRight</option><option>middleLeft</option><option>middleCenter</option><option>middleRight</option><option>bottomLeft</option><option>bottomCenter</option><option>bottomRight</option></select></div>\
		<div class="setting"><div class="setting_label">Final:</div>\
		<select class="textinput"><option>topLeft</option><option>topCenter</option><option>topRight</option><option>middleLeft</option><option>middleCenter</option><option>middleRight</option><option>bottomLeft</option><option>bottomCenter</option><option>bottomRight</option></select></div>\
		<h4>MISC</h4>\
		<div class="setting"><div class="setting_label">Link:</div><input type="text" id="text1" class="short textinput" name="firstname" value=""/></div>\
		<div class="setting"><div class="setting_label">Target:</div>\
		<select class="textinput short"><option>_blank</option><option>_self</option></select></div>\
		<div class="setting"><div class="setting_label">Caption:</div>\
		<input type="text" id="text1" class="textinput long zs2-title-main" name="firstname" value=""/></div>\
		<br />\
		<div class="setting"><div class="setting_label">Is header?</div><input type="checkbox" class="textinput iphoneit"/></div><br>\
		<div class="setting"><div class="setting_label">Bottom Right Append:</div><input type="text" id="text1" class="short textinput" name="firstname" value=""/><span class="sidenote">have a description in the bottom right <a href="'+window.zs2_path+'img/si1.png" class="thickbox" >like this</a></span></div><div class="clear"></div>\
        <div class="setting"><div class="setting_label">Preview Image:</div><input type="text" id="text1" class="textinput long zs2-file-input" name="firstname" value=""/>' + upload_button_string + '<span class="sidenote">Only for <strong>video, youtube or vimeo</strong> type</div>\
        <br><br><div class="sidenote">*if you want to upload your own video it must be in .flv (recommended) or .mp4 format<br>\
        *all the fields can be empty, thumbs will update automatically from YouTube if left empty</div>\
		</div>\
		</div>';


	jQuery('.zs2-slider').eq(argslider).find('.zs2-slider-thumbs').append('<div class="img-con"><img width="90" height="90" class="preview-img"/><div class="btn-img-delete"></div></div>')



	if(top==undefined)
	jQuery('.zs2-slider').eq(argslider).find('.item-con').append(adminItem);
	else
	jQuery('.zs2-slider').eq(argslider).find('.item-con').prepend(adminItem);

	zs2_arraySliders[argslider][0]++;


	


	}
	
	
	

	zs2_makeButtons();
}

function zs2_checkItem(argslider,arg1,arg2, value){
	//console.log(value)
	$cache = jQuery('.zs2-slider').eq(argslider).find('.item').eq(arg1).find('.textinput').eq(arg2);
	if(value!=''){
		if($cache[0].nodeName=='SELECT'){

			for(j=0;j<$cache.children().length;j++)
			if($cache.children().eq(j).text()==value)
			$cache.children().eq(j).attr('selected', 'selected');
			
			if($cache.hasClass('zs2-type-select')==true)
			if(value=='inline'){
			zs2_change_source(argslider, arg1-1, 80);
			}
			
			
			
			
		
		}
		else{
			if($cache.attr('type')=='checkbox'){
				if(value=='on');
				$cache.click();
				$cache.attr('checked', 'checked');
			}else{
			$cache.val(value);
				
			}
			
			
			
			
			if($cache.hasClass('zs2-file-input'))
			    zs2_changePreview($cache)

			var aux = $cache.hasClass('zs2-thumb-main');

			if (aux == true)
			    zs2_changePreview(argslider, arg1 - 1, value);
			

			var aux2 = $cache.hasClass('zs2-title-main');

			if (aux2 == true)
			    zs2_changeTitle(argslider, arg1 - 1, value);
		}
			
		

	
	}
}
function zs2_makeSortable(){
	jQuery( ".item-con" ).sortable({
		axis:   'y',
		containment: 'parent',
		tolerance: 'pointer',
		placeholder: "ui-state-highlight",
		update: zs2_handle_sortable_update
	});
};

function zs2_handle_sortable_update(){
	//console.log(zs2_currSlider.find('.item').length)
	for(i=1;i<zs2_currSlider.find('.item').length;i++){
	//console.log(i, zs2_currSlider.find('.item').eq(i).find('.zs2-thumb-main').val());
	//console.log
	zs2_changePreview(zs2_currSliderNr, i-1, zs2_currSlider.find('.item').eq(i).find('.zs2-thumb-main').val())
	}
}


function zs2_presendGallery(){
	if(cauta==''){
	zs2_sendGallery();
	}else{
	zs2_sendGallery();
	}
}
function zs2_sendGallery(){
	
	jQuery('#save-ajax-loading').css('visibility','visible');

	var i=0;
	var j=0;
	var k=0;
	var tempValue='';
	
	var mainString='';
	var optionsString='';
	var itemsString='';
	
	
	for(k=0;k<jQuery('.zs2-slider').length;k++){
	itemsString='';
	
	
	
	
	for(i=0;i<jQuery('.zs2-slider').eq(k).find('.item').length;i++){
		
		
		for(j=0;j<jQuery('.zs2-slider').eq(k).find('.item').eq(i).find('.textinput').length;j++){
		
		
		//console.log(k, i, j, $cache);
		$cache = jQuery('.zs2-slider').eq(k).find('.item').eq(i).find('.textinput').eq(j);
		if($cache[0].nodeName=="SELECT")
		tempValue = $cache.find(':selected').text();
		
		if($cache[0].nodeName=="INPUT")
		tempValue = $cache.val();
		if($cache[0].nodeName=="TEXTAREA")
		tempValue = $cache.val();
		
		if($cache[0].nodeName=="INPUT" && $cache.attr('type')=='checkbox'){
		tempValue='';
		if($cache.attr('checked')=='checked')
		tempValue='on';
		}
		itemsString+=tempValue + '&,';
		}//end j for
		//itemsString = itemsString.slice(0,itemsString.length-2);
		
		itemsString+='&;';
		}//end i for
		
		
		//console.log(itemsString);
		//itemsString = itemsString.slice(0,itemsString.length-2);
		//console.log(itemsString);
		
		
		mainString+=itemsString + '&.';
	}//end k for
	mainString = mainString.slice(0,mainString.length-2);
	
	//console.log(mainString);
	

		var data = {
			action: 'zs2_ajax',
			arrayMain: mainString
		};

		// since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
		jQuery.post(ajaxurl, data, function(response) {
			jQuery('#save-ajax-loading').css('visibility','hidden');
		});
}





zs2_adminSlider = '<div class="zs2-slider" style="display:none">\
<div class="zs2-slider-thumbs"></div><div class="clearfix"></div>\
	<div class="item">\
    <h3>OPTIONS</h3>\
    <div class="settings_cont">\
	<div class="setting"><div class="setting_label">Title [ ID ]:</div><input type="text" class="short textinput zs2-input-id" value="default" name="firstname"/><span class="sidenote">Must be unique</span></div>\
	<div class="setting"><div class="setting_label">Width:</div><input type="text" class="short textinput" name="firstname" value="600"/><span class="sidenote">In pixels</span></div>\
	<div class="setting"><div class="setting_label">Height:</div><input type="text" class="short textinput" name="firstname" value="400"/></div>\
	<div class="setting"><div class="setting_label">Transition Type:</div><select class="textinput short"><option>random</option><option>fade</option><option>wipe</option><option>strips_horizontal_simple</option><option>strips_horizontal_fade_simple</option><option>strips_horizontal_fade_random</option><option>strips_vertical_simple</option><option>strips_vertical_fade_simple</option><option>strips_vertical_fade_random</option><option>slide</option><option>swipe</option></select></div>\
	<div class="setting"><div class="setting_label">Navigation Type:</div><select class="textinput short"><option>thumbs</option><option>arrows</option><option>numbers</option><option>thumbsarrows</option><option>thumbsnumbers</option><option>none</option></select></div>\
	<h3>THUMBS</h3>\
	<div class="setting"><div class="setting_label">Thumbs Position:</div><select class="textinput short"><option>down</option><option>right</option><option>left</option><option>up</option></select></div>\
	<div class="setting"><div class="setting_label">Width:</div><input type="text" class="short textinput" name="firstname" value="100"/></div>\
	<div class="setting"><div class="setting_label">Height:</div><input type="text" class="short textinput" name="firstname" value="75"/></div>\
	<div class="setting"><div class="setting_label">Space Between:</div><input type="text" class="short textinput" name="firstname" value="10"/></div>\
	<h3>OTHER</h3>\
	<div class="setting"><div class="setting_label">Images Autoresize:</div><select class="textinput short"><option>off</option><option>on</option></select></div>' +
'	<div class="setting"><div class="setting_label">Strips X:</div><input type="text" class="short textinput" name="firstname" value="15" maxlength="2"/></div>\
	<div class="setting"><div class="setting_label">Strips Y:</div><input type="text" class="short textinput" name="firstname" value="15" maxlength="2"/></div>\
	<div class="setting"><div class="setting_label">Shadow:</div><select class="textinput short"><option>off</option><option>on</option></select></div>\
	<div class="setting"><div class="setting_label">Load on:</div><select class="textinput short"><option>Document Ready</option><option>Document Loaded</option></select><span class="sidenote">Document Loaded means that the gallery loads when all the page is loaded, useful only if you use Ken Burns effect.</span></div>\
	<div class="setting"><div class="setting_label">Load from:</div><select class="textinput short"><option>items</option><option>posts</option><option>flickr</option></select><span class="sidenote">Choose wheter or not the gallery should load from your posts ( check documentation )</span></div>\
	<div class="setting"><div class="setting_label">Auto set height:</div><select class="textinput short"><option>off</option><option>on</option></select><span class="sidenote">Auto set height depending on content ( only for load from posts )</span></div>\
	<div class="toggle"><div class="toggle-title"><h3><div class="arrowdown"></div>FLICKR Settings</h3></div><div class="toggle-content" style="display: none;"><div class="arrow-top"></div><div class="the-content"><div class="sidenote">Options only available if you choose Load from : flickr option. If this is your set link http:/'+"/"+'www.flickr.com/photos/60070586@N05/sets/<strong>72157626626923475</strong>, the bolded part is the ID\
	<div class="setting"><div class="setting_label">API Key:</div><input type="text" class="long textinput"/><span class="sidenote">Get a API key from <a href="http://www.flickr.com/services/api/misc.api_keys.html">here</a> if you dont have one.</span></div>\
	<div class="setting"><div class="setting_label">SET ID:</div><input type="text" class="long textinput"/></div></div></div></div><br>\
	<div class="setting"><div class="setting_label">Pause on Roll Over?</div><select class="textinput short"><option>on</option><option>off</option></select><span class="sidenote">Pause slideshow on roll over..</span></div>\
	<div class="setting"><div class="setting_label">Use Arrows?</div><select class="textinput short"><option>on</option><option>off</option></select></div>\
	<div class="setting"><div class="setting_label">Navigation Space:</div><input type="text" class="short textinput" name="firstname" value="25"/><span class="sidenote">Space between the images and navigation ( only for thumbs )..</span></div>\
	<div class="setting"><div class="setting_label">Navigation Arrow Size:</div><input type="text" class="short textinput" name="firstname" value="40"/><span class="sidenote">Navigation Arrows Size ( only for thumbs )..</span></div>\
	<div class="setting"><div class="setting_label">Arrows Normal Alpha:</div><input type="text" class="veryshort textinput" name="firstname" value="0.5"/><span class="sidenote">Value between 0 and 1..</span></div>\
	<div class="setting"><div class="setting_label">Arrows Roll Alpha:</div><input type="text" class="veryshort textinput" name="firstname" value="1"/><span class="sidenote">Value between 0 and 1..</span></div>\
	<div class="setting"><div class="setting_label">Thumbs Normal Alpha:</div><input type="text" class="veryshort textinput" name="firstname" value="0.5"/><span class="sidenote">Value between 0 and 1..</span></div>\
	<div class="setting"><div class="setting_label">Thumbs Roll Alpha:</div><input type="text" class="veryshort textinput" name="firstname" value="1"/><span class="sidenote">Value between 0 and 1..</span></div>\
	<div class="setting"><div class="setting_label">Use Arrows ?:</div><select class="textinput short"><option>on</option><option>off</option></select><span class="sidenote">Only for thumbs navigation type</span></div>\
	<div class="setting"><div class="setting_label">Navigate by mouse:</div><select class="textinput short"><option>off</option><option>on</option></select><span class="sidenote">Only for thumbs navigation type</span></div>\
	<div class="setting"><div class="setting_label">Navigation offset init:</div><input type="text" class="veryshort textinput" name="firstname" value="0"/><span class="sidenote">Only for thumbs navigation type</span></div>\
	<div class="setting"><div class="setting_label">Auto Center Images ?</div><select class="textinput short"><option>off</option><option>on</option></select><span class="sidenote">Auto center images of different sizes?</span></div>\
	</div>\
	</div>\
	</div><!--end item-->\
	<div class="item-con">\
	</div><!--end item con-->\
	<br><button class="button-secondary action kb-button-add-bot">Add Item</button>\
	</div>'








